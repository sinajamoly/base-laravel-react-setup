<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class SampleController extends Controller
{
    public function index()
    {
        return \response()->json(
            [
                'date' => ['success' => true]
            ]
        )->setStatusCode(Response::HTTP_OK);
    }
}
