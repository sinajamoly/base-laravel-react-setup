<?php

use App\Http\Controllers\SampleController;
use Illuminate\Support\Facades\Route;

// this is added here with the mindset of having more separation of concerns
Route::group(['prefix' => 'sample'], static function () {
    Route::get('/', SampleController::class . '@index');
});
