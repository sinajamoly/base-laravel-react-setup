import React from 'react';
import App from '@/app';
import ReactDOM from "react-dom";
import 'antd/dist/antd.css';

if (document.getElementById('root')) {
    ReactDOM.render(<App/>, document.getElementById('root'));
}
