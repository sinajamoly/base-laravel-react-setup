<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
{{--    this is the place i am going to replace the react rendered stuff--}}
    <div id="root">
        waiting to load, made by sina jamoly
{{--        this can be loaded fancy base on need--}}
    </div>

    <!-- React JS -->
    <script src="{{ asset('js/index.js') }}" defer>

    </script>
</body>
</html>
