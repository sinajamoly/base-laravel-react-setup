const mix = require('laravel-mix');

// this has been changes by Sina, just personal preference, make it consistence with npx create-react-app for index.js
mix.webpackConfig({
        resolve: {
            extensions: [".js", ".jsx"],
            alias: {
                "@": __dirname + "/resources/js"
            }
        }
    })
    .react('resources/js/index', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
